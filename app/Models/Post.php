<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    use HasFactory;

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo(User::modelClass());
    }

    /**
     * 
     */
    public function create($data)
    {
        \Log::debug($data);
        $this->title = $data['title']['_value'];
        $this->slug = $slug = Str::slug($data['title']['_value'], '-');
        $this->body = $data['body']['_value'];
        if ($data['anonymus'] != 'on') {
            $this->user_id = Auth::id();
        }
        return $this->save();
    }
}
